<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{
    // use HasFactory; cria um componentes para teste, preenche campos e tabelas
    protected $table = "candidatos";

    protected $fillable = [
        'nome',
        'sexo',
        'cpf',
        'rg',
        'emissor',
        'estado_civil',
        'mae',
        'pai',
        'naturalidade',
        'nacionalidade',
        'data_nasc',
        'crm',
        'pcd'
    ];
}
