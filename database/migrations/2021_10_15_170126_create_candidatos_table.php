<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatos', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('sexo')->comment('1-Masculino, 2-Feminino, 3-Não especificado');
            $table->string('cpf');
            $table->string('rg');
            $table->string('emissor');
            $table->integer('estado_civil')->comment('1-solteiro, 2-casado, 3-união estavel, 4-viuvo,5-divorciado');
            $table->string('mae');
            $table->string('pai');
            $table->string('naturalidade');
            $table->string('nacionalidade');
            $table->date('data_nasc');
            $table->string('crm');
            $table->boolean('pcd')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatos');
    }
}
